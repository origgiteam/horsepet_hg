import Vue from "vue";
import VueRouter from "vue-router";
import HomePage from "../views/HomePage.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: HomePage
  },
  {
    path: "/prodotti/:id/:titolo",
    //path: "/prodotti",
    name: "prodotti",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "prodotti" */ "../views/ListaProdotti.vue")
  },
  {
    path: "/prodotto/:id/:titolo",
    name: "prodotto",
    component: () =>
      import(
        /* webpackChunkName: "prodotto" */ "../views/DettaglioProdotto.vue"
      )
  },
  {
    path: "/carrello",
    name: "carrello",
    component: () =>
      import(/* webpackChunkName: "carrello" */ "../views/CarrelloUtente.vue")
  }
];

const router = new VueRouter({
  //mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
