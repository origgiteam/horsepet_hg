import axios from "axios";

const apiClient = axios.create({
  baseUR: "",
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-type": "application/json"
  }
});

export default {
  getEvents() {
    return apiClient.get("/events");
  }
};
