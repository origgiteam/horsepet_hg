export const funzioni = {
  methods: {
    //trasforma il titolo per URL user friendly
    string_to_slug(str) {
      str ? str : (str = "");
      str = str.replace(/^\s+|\s+$/g, ""); //trim
      str = str.toLowerCase();

      //rimuovo accenti e caratteri speciali
      var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
      var to = "aaaaeeeeiiiioooouuuunc------";
      for (var i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
      }

      str = str
        .replace(/[^a-z0-9 -]/g, "") //rimuovo caratteri non alfanumerici
        .replace(/\s+/g, "-") //sostituisco gli spazi con -
        .replace(/-+/g, "-"); //rimuovo i - multipli

      return str;
    },

    scegliTitolo(tit1, tit2) {
      let titolo = "";
      tit1 ? (titolo = tit1) : (titolo = tit2);
      return titolo;
    },

    opzioneSelezionata(val1, val2) {
      let risultato = "";
      val1 == val2 ? (risultato = "selected") : risultato;
      return risultato;
    }
  }
};
